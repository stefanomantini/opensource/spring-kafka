# Spring Kafka Samples

### Examples:
* JSON && Avro Listeners
* KTable - `UserTable`
* Streams (and table join) - `DeliveryNotificationStream`
* Spring Retry (See `RetryListenerSpec`)
* Streams Testing (See `UserTableSpec` && `DeliveryNotificationStreamSpec`)
* Embedded Testing (See `UserTableEmbeddedSpec` && `DeliveryNotificationEmbeddedSpec`)
* `MockSchemaRegistryClient` usage (see all tests)
