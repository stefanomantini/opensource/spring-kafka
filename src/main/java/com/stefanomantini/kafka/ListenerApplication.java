package com.stefanomantini.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;

@EnableKafka
@EnableKafkaStreams
@SpringBootApplication
public class ListenerApplication {

  public static void main(final String[] args) {
    SpringApplication.run(ListenerApplication.class, args);
  }
}
