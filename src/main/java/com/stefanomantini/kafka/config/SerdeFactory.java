package com.stefanomantini.kafka.config;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.common.serialization.Serde;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SerdeFactory {
  /**
   * Generates a SpecifcAvroSerde with the provided props. This method will assume that the Serde is
   * for the Value and not the Key.
   *
   * @param props avro props (see AbstractKafkaAvroSerDeConfig)
   * @param <T> Type of AvroSerde
   * @return SpecificAvroSerde
   */
  public <T extends SpecificRecord> Serde<T> createSpecificSerde(final Map<String, ?> props) {
    return this.createSpecificSerde(props, false, null);
  }

  /**
   * Generates a SpecifcAvroSerde with the provided props. This method will assume that the Serde is
   * for the Value and not the Key.
   *
   * @param props avro props (see AbstractKafkaAvroSerDeConfig)
   * @param <T> Type of AvroSerde
   * @return SpecificAvroSerde
   */
  <T extends SpecificRecord> Serde<T> createSpecificSerde(
      final Map<String, ?> props, final SchemaRegistryClient schemaRegistryClient) {
    return this.createSpecificSerde(props, false, schemaRegistryClient);
  }

  /**
   * Generates a SpecifcAvroSerde with the provided props.
   *
   * @param props avro props (see AbstractKafkaAvroSerDeConfig)
   * @param isKey whether or not the serde is for a key or value
   * @param <T> Type of AvroSerde
   * @return SpecificAvroSerde
   */
  private <T extends SpecificRecord> Serde<T> createSpecificSerde(
      final Map<String, ?> props,
      final boolean isKey,
      final SchemaRegistryClient schemaRegistryClient) {
    final Serde<T> serde;
    if (schemaRegistryClient != null) {
      serde = new SpecificAvroSerde<>(schemaRegistryClient);
    } else {
      serde = new SpecificAvroSerde<>();
    }
    serde.configure(props, false);
    return serde;
  }
}
