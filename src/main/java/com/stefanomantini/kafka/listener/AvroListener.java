package com.stefanomantini.kafka.listener;

import com.stefanomantini.kafka.avro.AvroSampleData;
import com.stefanomantini.kafka.domain.ImportantData;
import com.stefanomantini.kafka.service.ImportantDataService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AvroListener {
  private final ImportantDataService dataService;

  public AvroListener(final ImportantDataService importantDataService) {
    this.dataService = importantDataService;
  }

  @KafkaListener(topics = "${topics.example-data}", containerFactory = "avroListenerFactory")
  public void listen(
      final ConsumerRecord<String, AvroSampleData> record, final Acknowledgment acks) {
    log.info("received: key={}, value={}", record.key(), record.value());

    // convert avro data to important data
    final AvroSampleData sampleData = record.value();
    final ImportantData data = new ImportantData();
    data.setId(sampleData.getId());
    data.setName(sampleData.getName());
    data.setDescription("this is avro data");

    this.dataService.syncData(data);

    acks.acknowledge();
    log.info("message acknowledged.");
  }
}
