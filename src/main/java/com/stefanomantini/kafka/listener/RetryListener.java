package com.stefanomantini.kafka.listener;

import com.stefanomantini.kafka.domain.ImportantData;
import com.stefanomantini.kafka.service.ImportantDataService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RetryListener {
  private final ImportantDataService dataService;

  public RetryListener(final ImportantDataService service) {
    this.dataService = service;
  }

  @KafkaListener(
      topics = "${topics.retry-data}",
      containerFactory = "kafkaListenerContainerFactory")
  public void listen(
      final ConsumerRecord<String, ImportantData> record, final Acknowledgment acks) {
    log.info("received: key={}, value={}", record.key(), record.value());
    this.dataService.syncData(record.value());
    acks.acknowledge();
    log.info("message acknowledged.");
  }
}
