package com.stefanomantini.kafka.service;

import com.stefanomantini.kafka.domain.ImportantData;

public interface ImportantDataService {
  void syncData(ImportantData data);
}
