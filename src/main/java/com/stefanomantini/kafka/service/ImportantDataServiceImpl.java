package com.stefanomantini.kafka.service;

import com.stefanomantini.kafka.domain.ImportantData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ImportantDataServiceImpl implements ImportantDataService {
  @Override
  public void syncData(final ImportantData data) {
    log.info("syncing id={}, name={}", data.getId(), data.getName());
  }
}
